import React, { Component } from 'react';
import './App.scss';
import Logo from '../images/logo-600x340.png';
import Breed from '../components/Breed/Breed';
import SubBreed from '../components/SubBreed/SubBreed';
import GetBreeds from '../services/breeds/getBreeds';
import GetDogs from '../services/dogs/getDogs';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      breeds: [],
      dogImage: ''
    };
  }

  async componentDidMount() {
    await this.getAllBreeds();
  }

  async getAllBreeds() {
    new GetBreeds().getAllBreeds((resp) => {
      let breedsArray = [];

      for (let prop in resp.message) {
        if ((resp.message).hasOwnProperty(prop)) {
          let innerObj = {};
          
          innerObj[prop] = resp.message[prop];

          breedsArray.push({
            name: Object.keys(innerObj)[0],
            subBreeds: Object.values(innerObj)[0]
          });
        }
      }

      breedsArray.forEach(function(v, i){
        v['subBreedsTmp'] = [];

        v.subBreeds.forEach(function(j, k){
          v.subBreedsTmp.push({
            name: j
          });
        });

        v.subBreeds = v.subBreedsTmp;
        delete v.subBreedsTmp;
      });

      this.setState({
        breeds: breedsArray
      });
    });
  }

  /**
   * Get random dog image by breed or sub-breed
   * @param {Object} obj 
   * @param {string} breedName 
   */
  getRandomDogs(obj, breedName) {
    if(typeof breedName === "undefined"){
      //Breed
      new GetDogs().getRandomDogFromBreed(obj.name, (resp) => {
        this.setState({
          dogImage: resp.message
        });
      }, (error) => {
        console.log(error.message);
      });
    }else{
      //SubBreed
      new GetDogs().getRandomDogFromSubBreed(breedName, obj.name, (resp) => {
        this.setState({
          dogImage: resp.message
        });
      }, (error) => {
        console.log(error.message);
      });
    }
  }

  render() {
    return (
      <div className="app">
        <header className="app-header">
          <img src={Logo} className="app-logo" alt="logo" />
          <h1 className="app-title">FrontEnd Test</h1>
        </header>
        <div className="app-subheader">
          <p>Select a breed or <span className="subBreed-color">sub-breed</span> from the list on the left</p>
        </div>
        <div className="app-container">
          <div className="app-container-left">
            <div className="breeds-container">
              <div className="breeds-container-header">
              </div>
              <div className="breeds-container-list">
                <ul>
                  {this.state.breeds.map((breed, i) => {
                    return ([
                      <Breed data={breed} key={i} onClick={() => this.getRandomDogs(breed)} />,
                      breed.subBreeds.map((subBreed, k) => 
                        <SubBreed data={subBreed} parent={breed} key={k} onClick={() => this.getRandomDogs(subBreed, breed.name)} />
                      ),
                    ]);
                  })}
                </ul>
              </div>
            </div>
          </div>
          <div className="app-container-right">
            <div className="dog-image-container" style={{backgroundImage: `url(${this.state.dogImage})`}}>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
