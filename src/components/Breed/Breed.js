import React, { Component } from 'react';
import './Breed.scss';

class Breed extends Component {
  render() {
    return (
      <li className="breed-item" onClick={this.props.onClick}>
        <span className="breed-item-name">{this.props.data.name}</span><br/>
      </li>
    );
  }
}

export default Breed;
