import React, { Component } from 'react';
import './SubBreed.scss';

class SubBreed extends Component {
  render() {
    return (
      <li className="subBreed-item" onClick={this.props.onClick}>
        <span>{this.props.data.name} {this.props.parent.name}</span>
      </li>
    );
  }
}

export default SubBreed;
