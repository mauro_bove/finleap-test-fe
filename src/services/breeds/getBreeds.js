import MainService from '../mainService';

export default class GetBreeds extends MainService {
    
    /**
     * @param {Function} thenFn - callback
     * @param {Function} catchFn - callback
     */
    getAllBreeds = (thenFn, catchFn) => {
        this.get('breeds/list/all', res => {
            if(res.status === 'success'){
                thenFn(res);
            }else{
                catchFn(res);
            }
        })
    };
}