import MainService from '../mainService';

export default class GetDogs extends MainService {

    /**
     * @param {string} breedName
     * @param {Function} thenFn - callback
     * @param {Function} catchFn - callback
     * 
     * @memberof GetDogs
     */
    getRandomDogFromBreed = (breedName, thenFn, catchFn) => {
        this.get(`breed/${breedName}/images/random`, res => {
            if(res.status === 'success'){
                thenFn(res);
            }else{
                catchFn(res);
            }
        })
    };

    /**
     * @param {string} breedName
     * @param {string} subBreedName
     * @param {Function} thenFn - callback
     * @param {Function} catchFn - callback 
     * 
     * @memberof GetDogs
     */
    getRandomDogFromSubBreed = (breedName, subBreedName, thenFn, catchFn) => {
        this.get(`breed/${breedName}/${subBreedName}/images/random`, res => {
            if(res.status === 'success'){
                thenFn(res);
            }else{
                catchFn(res);
            }
        })
    };
}