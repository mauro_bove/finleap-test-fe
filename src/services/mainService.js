const BASE_URL = 'https://dog.ceo/api';

export default class MainService {
    /**
     * @param {string} method - API method
     * @param {string} path - API url
     * @param {Object} headers - Request headers
     * @param {Object} body - Body request (only for POST methodes)
     */
    getRequest = (method = 'GET', path = '', headers = {}, body = JSON.stringify({})) => {
        let request;
        let config = {
            method: method,
            mode: 'cors',
            redirect: 'follow',
            headers: headers,
            body: body
        };
        if(method === 'GET') delete config.body;
        request = new Request(`${BASE_URL}/${path}`, config);
        return request;
    };

    /**
     * @param {Function} request
     * @param {Function} thenFn - callback
     */
    getCall = (request, thenFn) => {
        fetch(request)
            .then((response) => response.json())
            .then(thenFn)
    };

    /**
     * @param {string} path - API url
     * @param {Function} thenFn - callback
     */
    get = (path, thenFn) => {
        let request = this.getRequest('GET', path);
        this.getCall(request, thenFn);
    };
}